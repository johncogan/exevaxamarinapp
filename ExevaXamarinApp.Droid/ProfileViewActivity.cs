﻿using Android.App;
using MvvmCross.Droid.Views;
using ExevaXamarinApp.ViewModels;

namespace ExevaXamarinApp.Droid
{

	[Activity(Label = "ProfileViewActivity", MainLauncher = true,Theme = "@android:style/Theme.NoTitleBar")]
	public class ProfileViewActivity : MvxActivity
	{
        public new ProfileViewModel ViewModel
		{
			get { return (ProfileViewModel)base.ViewModel; }
			set { base.ViewModel = value; }
		}

		protected override void OnViewModelSet()
		{
			SetContentView(Resource.Layout.View_Profile);
		}
	}
}
