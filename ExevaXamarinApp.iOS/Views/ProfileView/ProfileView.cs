﻿using MvvmCross.iOS.Views;
using MvvmCross.Binding.BindingContext;
using MBProgressHUD;
using UIKit;

using ExevaXamarinApp.ViewModels;
using System;
using Foundation;
using System.Collections.Generic;
using MvvmCross.Binding.iOS.Views;
using ExevaXamarinApp.Services;

namespace ExevaXamarinApp.iOS.Views.ProfileView
{
    public partial class ProfileView : MvxViewController
    {
        private BindableProgress _bindableProgress;

        public ProfileView() : base("ProfileView", null)
        {
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            _bindableProgress = new BindableProgress(View);

            // Set up Sex picker list
            var pickerViewModel = new MvxPickerViewModel(sex);
			sex.Model = pickerViewModel;
			sex.ShowSelectionIndicator = true;

            // Start binding the controls
            var set = this.CreateBindingSet<ProfileView, ViewModels.ProfileViewModel>();

            // Field binding
			set.Bind(firstName).To((ProfileViewModel vm) => vm.FirstName);
			set.Bind(lastName).To(vm => vm.LastName);
            set.Bind(dateOfBirth).To((ProfileViewModel vm) => vm.DateOfBirth);

            set.Bind(description).To((ProfileViewModel vm) => vm.Description);

            // DatePicker and List Picker
            // TODO If there is time bind these using Mvx methods
			set.Bind(pickerViewModel).For(p => p.ItemsSource).To(vm => vm.ListGender);
			set.Bind(pickerViewModel).For(p => p.SelectedItem).To(vm => vm.Gender);

            // Command binding
            set.Bind(btnSave).To((ProfileViewModel vm) => vm.SaveProfileCommand);
            set.Bind(btnClear).To((ProfileViewModel vm) => vm.ClearFields);

            // Activity spinner binding
            set.Bind(_bindableProgress).For(b => b.Visible).To(vm => vm.IsBusy);

            // Finalise the bindings
            set.Apply();

            // Add limits for string lengths
            limitTextFieldLength(firstName);
            limitTextFieldLength(lastName);

            // UITextView does it slightly differently
            description.ShouldChangeText += CheckTextViewLength;

            // We want keyboard to disappear when focus is outside the text controls
			View.AddGestureRecognizer(new UITapGestureRecognizer(() =>
			{
				firstName.ResignFirstResponder();
				lastName.ResignFirstResponder();
                description.ResignFirstResponder();
			}));
        }

        /// <summary>
        /// Checks the length of the text view and limits it
        /// </summary>
        /// <returns><c>true</c>, if text view length was checked, <c>false</c> otherwise.</returns>
        /// <param name="textView">Text view.</param>
        /// <param name="range">Range.</param>
        /// <param name="text">Text.</param>
		private bool CheckTextViewLength(UITextView textView, NSRange range, string text)
		{
			return textView.Text.Length + text.Length - range.Length <= 500;
		}

        /// <summary>
        /// Limits the length of the text field.
        /// </summary>
        /// <param name="_tField">T field.</param>
        private void limitTextFieldLength(UITextField _tField){
			UITextField targetField = _tField;
			targetField.ShouldChangeCharacters = (textField, range, replacementString) =>
			{
				var newLength = textField.Text.Length + replacementString.Length - range.Length;
				return newLength <= 50;
			};
        }
}



    /// <summary>
    /// Bindable activity spinner to show when we are doing something in the background
    /// </summary>
    public class BindableProgress
    {
        private MTMBProgressHUD _progress;
        private UIView _parent;

        public bool Visible{
            get{
                return _progress != null;
            }
            set{
                if (Visible == value)
                    return;

                if(value)
                {
                    _progress = new MTMBProgressHUD(_parent)
                    {
                        LabelText = "Saving profile to cloud...",
                        RemoveFromSuperViewOnHide = true
                    };
                    _parent.AddSubview(_progress);
                    _progress.Show(true);
                }else{
                    _progress.Hide(true);
                    _progress = null;
                }
            }
        }

        public BindableProgress(UIView parent)
        {
            _parent = parent;
        }
    }


}

