// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ExevaXamarinApp.iOS.Views.ProfileView
{
	[Register ("ProfileView")]
	partial class ProfileView
	{
		[Outlet]
		UIKit.UIButton btnClear { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIKit.UIButton btnSave { get; set; }

		[Outlet]
		UIKit.UIDatePicker dateOfBirth { get; set; }

		[Outlet]
		UIKit.UITextView description { get; set; }

		[Outlet]
		UIKit.UITextField firstName { get; set; }

		[Outlet]
		UIKit.UITextField lastName { get; set; }

		[Outlet]
		UIKit.UIPickerView sex { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnClear != null) {
				btnClear.Dispose ();
				btnClear = null;
			}

			if (dateOfBirth != null) {
				dateOfBirth.Dispose ();
				dateOfBirth = null;
			}

			if (firstName != null) {
				firstName.Dispose ();
				firstName = null;
			}

			if (lastName != null) {
				lastName.Dispose ();
				lastName = null;
			}

			if (sex != null) {
				sex.Dispose ();
				sex = null;
			}

			if (btnSave != null) {
				btnSave.Dispose ();
				btnSave = null;
			}

			if (description != null) {
				description.Dispose ();
				description = null;
			}
		}
	}
}
