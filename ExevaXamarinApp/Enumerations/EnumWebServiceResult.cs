﻿﻿using System;

namespace ExevaXamarinApp.Services
{
    public enum EnumWebServiceResult
    {
        SUCCESS,
        FAILED_UNKNOWN,
        NO_INTERNET_CONNECTION,
        INVALID_DATA
    };
}
