﻿using System;
using ExevaXamarinApp.ViewModels;
using MvvmCross.Platform;

namespace ExevaXamarinApp
{
    public static class IoCRegistrations
    {
        
        public static void InitIoC(){
            // Register view models to allow retrival of registered 
            // interfaces within view-model constructors
            Mvx.IocConstruct<ProfileViewModel>();
        }
    }
}
