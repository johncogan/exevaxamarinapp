﻿using System;
using System.Threading.Tasks;
using ExevaXamarinApp.Services;

namespace ExevaXamarinApp.Models
{
    public class Profile
    {
		public string firstName { get; set; }
		public string lastName { get; set; }
        public DateTime? dateOfBirth { get; set; }
		public EnumSex? sex { get; set; }
		public string description { get; set; }

        public Profile()
        {
            firstName = "";
            lastName = "";
            dateOfBirth = null;
            sex = EnumSex.MALE;
            description = "";
        }

        /// <summary>
        /// Validates the data within the model
        /// </summary>
        /// <returns><c>true</c>, if valid data is valid, <c>false</c> otherwise.</returns>
        public bool IsValidData(){
            System.Boolean isValid = true;

            if(string.IsNullOrWhiteSpace(firstName)){
                isValid = false;
            }

			if (string.IsNullOrWhiteSpace(lastName))
			{
				isValid = false;
			}

            if(this.sex == null)
            {
                isValid = false;
            }

            // Check dateOfBirth has a value
            if (!dateOfBirth.HasValue){
                isValid = false;
            }


            return isValid;
        }

    }
}
