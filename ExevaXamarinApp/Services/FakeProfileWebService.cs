﻿using System.Threading.Tasks;
using ExevaXamarinApp.Models;

namespace ExevaXamarinApp.Services
{
    public class FakeProfileWebService : IProfileWebService
    {
        public int _delayPeriod { get; private set; } // TODO Remove when web service is done
		EnumWebServiceResult? _lastResult;

        public EnumWebServiceResult? getLastResult(){
            return _lastResult;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:ExevaXamarinApp.Enumerations.FakeProfileWebService"/> class.
        /// </summary>
        /// 3 second delay to simulate a remote request
        public FakeProfileWebService()
        {
            _lastResult = null;
            _delayPeriod = 3000; // TODO Remove when web service is done
        }

        /// <summary>
        /// Make thread wait.
        /// </summary>
        // TODO Remove when web service is done
		private async Task Sleep()                                
		{
            await Task.Delay(3000).ConfigureAwait(false);
		}

        // TODO When web service API complete use HttpClient to connect instead of faked methods.
        public void setupHttpClient(){
            throw new System.Exception("Not currently implemented");
        }

        /// <summary>
        /// Sends the profile to server asynchronously
        /// </summary>
        /// <returns>EnumWebServiceResultFlag value</returns>
        /// <param name="profileObject">Profile model object</param>
        // TODO: Currently faking - Complete when Web service is done
        public async Task SendProfileToServer(Profile profileObject)
        {
            _lastResult = null;

            // Validate arguments before attempting to use web serivce
            if (profileObject.IsValidData())
            {
                // TODO: Return ENUM FLAG that represents the state of the result

                // Simulate a POST request to web service
                await Sleep().ConfigureAwait(false);
                _lastResult = EnumWebServiceResult.SUCCESS;
            }else{
                _lastResult = EnumWebServiceResult.INVALID_DATA;
            }
        }
    }
}
