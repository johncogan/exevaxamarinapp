﻿using System;
using System.Threading.Tasks;
using ExevaXamarinApp.Models;

namespace ExevaXamarinApp.Services
{
    public interface IProfileWebService
    {
        Task SendProfileToServer(Profile profileObject);
        EnumWebServiceResult? getLastResult();
    }
}
