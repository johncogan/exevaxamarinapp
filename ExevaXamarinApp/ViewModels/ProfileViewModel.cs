﻿using MvvmCross.Core.ViewModels;
using System;
using System.IO;
using ExevaXamarinApp.Services;
using ExevaXamarinApp.Models;
using MvvmCross.Platform;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Collections.Generic;
using Chance.MvvmCross.Plugins.UserInteraction;

namespace ExevaXamarinApp.ViewModels
{
    /// <summary>
    /// Profile view model.
    /// </summary>
	public class ProfileViewModel : MvxViewModel
	{
        readonly IProfileWebService _profileWebService; // Web Service instance (FAKE)
        Profile _profile; // Model instane
        private bool _isBusy;

		/// <summary>
		/// Use constructor injection to provide access to the Profile Web Service
		/// </summary>
		public ProfileViewModel(IProfileWebService profileWebService)
		{
			_profileWebService = profileWebService;
		}

		#region Properties
		public bool IsBusy
		{
			get { return _isBusy; }
			set { _isBusy = value; RaisePropertyChanged(() => IsBusy); }
		}

        public string FirstName
		{
			get { return _profile.firstName; }

			set
			{
				// Only set IF the property has actually changed
				if (!value.Equals(_profile.firstName))
				{
					_profile.firstName = value;
					RaisePropertyChanged(() => FirstName);
				}
			}
		}

		public string LastName
		{
			get { return _profile.lastName; }

			set
			{
				// Only set IF the property has actually changed
				if (!value.Equals(_profile.lastName))
				{
					_profile.lastName = value;
					RaisePropertyChanged(() => LastName);
				}
			}
		}

        // Nullable DateTime
        public DateTime? DateOfBirth
		{
			get { return _profile.dateOfBirth; }

			set
			{
				// Only set IF the property has actually changed
				if (!value.Equals(_profile.dateOfBirth))
				{
                    _profile.dateOfBirth = value;
					RaisePropertyChanged(() => DateOfBirth);
				}
			}
		}

        public EnumSex? Gender
		{
            get { return _profile.sex; }
			set {
                if(value == EnumSex.MALE)
                    _profile.sex = EnumSex.MALE;

				if (value == EnumSex.FEMALE)
					_profile.sex = EnumSex.FEMALE;

				if (value == EnumSex.OTHER)
					_profile.sex = EnumSex.OTHER;
                
                RaisePropertyChanged(() => Gender); 
            }
		}

        private List<EnumSex?> _listGender = new List<EnumSex?>()
			{
				EnumSex.MALE, EnumSex.FEMALE, EnumSex.OTHER
			};

		public List<EnumSex?> ListGender
		{
			get { return _listGender; }
			set { 
                _listGender = value; 
                RaisePropertyChanged(() => ListGender); 
            }
		}

		public string Description
		{
            get { return _profile.description; }

			set
			{
				// Only set IF the property has actually changed
				if (!value.Equals(_profile.description))
                {
                    _profile.description = value;
					RaisePropertyChanged(() => Description);
				}
			}
		}
        #endregion

        #region Commands
        /// <summary>
        /// Command to clear all fields
        /// </summary>
        public ICommand ClearFields
        {
            get
            {
                return new MvxCommand(() =>
                {
                    _profile.firstName = "";
                    _profile.lastName = "";
                    _profile.sex = EnumSex.MALE;
                    _profile.dateOfBirth = null;
                    _profile.description = null;
                    RaiseAllPropertiesChanged();

                });

            }
        }

		public ICommand SaveProfileCommand
		{
			get
			{
				return new MvxCommand(async () => {
					if (_profile.IsValidData())
					{
                        IsBusy = true;
						// Wait for task to compelte, show busy indicator
						await Mvx.Resolve<IProfileWebService>().SendProfileToServer(_profile).ConfigureAwait(false); 

						if (_profileWebService.getLastResult() == EnumWebServiceResult.SUCCESS)
						{
							// TODO Display success message
						}

						if (_profileWebService.getLastResult() == EnumWebServiceResult.NO_INTERNET_CONNECTION)
						{
							// TODO Display no internet connection message
						}

						if (_profileWebService.getLastResult() == EnumWebServiceResult.FAILED_UNKNOWN)
						{
							// TODO Display unknown error message / exception
						}
                        IsBusy = false;

                        // Alert user that the profile was saved.
                        Mvx.Resolve<IUserInteraction>().Alert("Profile saved");
				    }
				});
			}
		}
        #endregion


        public void Init(Profile profile = null)
		{
            // Create a profile if it doesnt already exist.
            _profile = profile == null ? new Profile() : profile;
			RaiseAllPropertiesChanged();
		}

    }
}